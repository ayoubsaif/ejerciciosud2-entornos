package ejercicio1;

import java.util.Scanner;

public class Metodos01 {
	static Scanner input = new Scanner(System.in);

	// metodo que lee un vector de cadenas
	public static String metodo01() {
		System.out.println("Dime Algo:");
		String metodo1 = input.nextLine();

		return metodo1;
	}
	public static String metodo02() {
		System.out.println("Dime Algo:");
		String metodo2 = input.nextLine();

		return metodo2;
	}
	public static String metodo03() {
		System.out.println("Dime Algo:");
		String metodo3 = input.nextLine();

		return metodo3;
	}
}
